void init(int numberOfFrames,int numberOfReferences,int numberOfPagesPerProcess);
void run_FIFO();
void run_LRU();
void run_secondChance();
void run_improvedSecondChance();
void print_analytics();
void print_pages();
