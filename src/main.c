#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "../include/pageController.h"

int main(int argc, char** argv){
    int parameter;
    int v = 1024,s = 5000,f = 512;

    while((parameter = getopt(argc,argv,"v:s:f:")) != -1){
        switch(parameter)
            {
            case 'v':
                v = atoi(optarg);
                break;
            case 's':
                s = atoi(optarg);
                break;
            case 'f':
                f = atoi(optarg);
                break;
            default:
                printf("Erro nos parâmetros\nTente usar os parâmetros:\nsubpag -v <num_pags> -s <tam_sr> -f <num_frames>");
                exit(-1);
            }
    }

    init(v,s,f);
    printf("FIFO:                    ");
    run_FIFO();
    print_analytics();
//    print_pages();
    
    init(v,s,f);
    printf("LRU                      ");
    run_LRU();
    print_analytics();
//    print_pages();

    init(v,s,f);
    printf("Second Chance            ");
    run_secondChance();
    print_analytics();
//    print_pages();

    init(v,s,f);
    printf("Improved Second Chance:  ");
    run_improvedSecondChance();
    print_analytics();
//    print_pages();

    return 0;
}
