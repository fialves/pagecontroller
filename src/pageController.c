#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "../include/pageController.h"

#define PROB_FOR_ACCESS_TYPES 16
#define READ  0
#define WRITE 1

struct referenceString {
    struct referenceString* next;
    struct referenceString* prev;
    int reference;
    int accessType;
}typedef referenceString_t;

struct pageInMemory {
    struct pageInMemory* next;
    struct pageInMemory* prev;
    int page;
    int referenceBit;
    int modificationBit;
}typedef pageInMemory_t;

void    init_referencesString();
void    init_pagesInMemory();
int     search_page(referenceString_t* newReference);
void    swap_pages_FIFO(int newPage);
void    swap_pages_LRU(int newPage);
void    reorder_pages(int page);
void    swap_pagesForSecondChance(int newPage);
void    swap_pagesForImprovedSecondChance(referenceString_t* newReference);
void    reset_counters();

referenceString_t* referencesString;
pageInMemory_t* pagesInMemory;
pageInMemory_t* currentPage;

int numberOfPages,numberOfReferences,numberOfFrames;
int pageInCounter = 0,pageOutCounter = 0,pageFaultCounter = 0;


void init(int totalPagesPerProcess,int totalReferences,int totalFrames){
    numberOfReferences = totalReferences;
    numberOfPages = totalPagesPerProcess;
    numberOfFrames = totalFrames;
    init_referencesString();
    init_pagesInMemory();
}

// Inicializa referencesString como uma estrutura duplamente encadeada e circular 
// do tipo referenceString_t
void init_referencesString(){    
    // FIXME: verificar se é necessário ser duplamente encadeada
    srand(time(NULL));
    int randomReference,randomAccessType;
    referencesString = (referenceString_t*)malloc(sizeof(referenceString_t));
    if(referencesString == NULL) return;
    referenceString_t* currentReference = referencesString;

    randomReference = rand() % numberOfPages;
    currentReference->reference = randomReference;
    randomAccessType = rand() % PROB_FOR_ACCESS_TYPES;
    currentReference->accessType = (randomAccessType > 12 ? WRITE : READ);
//    printf("%d(%d)\n",currentReference->reference,currentReference->accessType);

    int index;
    for(index = 1; index < numberOfReferences; index++){
        currentReference->next = (referenceString_t*)malloc(sizeof(referenceString_t));
        if(currentReference->next == NULL) return;
        currentReference->next->prev = currentReference;
        currentReference = currentReference->next;
        randomReference = rand() % numberOfPages;
        currentReference->reference = randomReference;
        randomAccessType = rand() % PROB_FOR_ACCESS_TYPES;
        currentReference->accessType = (randomAccessType > 12 ? WRITE : READ);
//        printf("%d(%d)\n",currentReference->reference,currentReference->accessType);
    } 	
    currentReference->next = referencesString;
    referencesString->prev = currentReference;
}

// Inicializa pagesInMemory como um vetor de uma estrutura do tipo pageInMemory_t
void init_pagesInMemory(){
    int pageIndex;
    pagesInMemory = (pageInMemory_t*)malloc(sizeof(pageInMemory_t));
    if(pagesInMemory == NULL) return;    
    pageInMemory_t* currentPage = pagesInMemory;
    currentPage->page = -1;
    currentPage->referenceBit = 0;
    currentPage->modificationBit = 0;
    for(pageIndex = 1;pageIndex < numberOfFrames;pageIndex++){        
        currentPage->next = (pageInMemory_t*)malloc(sizeof(pageInMemory_t));
        if(currentPage->next == NULL) return;    
        currentPage->next->prev = currentPage;
        currentPage = currentPage->next;
        currentPage->page = -1;
        currentPage->referenceBit = 0;
        currentPage->modificationBit = 0;
    }
    currentPage->next = pagesInMemory;
    pagesInMemory->prev = currentPage;
}

// Famoso algoritmo First In First Out
void run_FIFO(){
    reset_counters();
    currentPage = pagesInMemory;
    referenceString_t* currentReference = referencesString;
    do{
//        printf(">%d\n",currentReference->reference);
        if(search_page(currentReference) == 1){                
            pageFaultCounter++;
            swap_pages_FIFO(currentReference->reference);   
            if(pageInCounter >= numberOfFrames)   
                pageOutCounter++;           
            pageInCounter++;
        }
        currentReference = currentReference->next;
    }while(currentReference != referencesString);
}

void run_LRU(){
    reset_counters();
    currentPage = pagesInMemory;
    referenceString_t* currentReference = referencesString;
    do {
//        printf(".%d\n",currentReference->reference);
        if(search_page(currentReference) == 1){                
            pageFaultCounter++;    
            swap_pages_LRU(currentReference->reference);         
            reorder_pages(currentReference->reference);
            if(pageInCounter >= numberOfFrames) 
                pageOutCounter++; // FIXME: pageOutCounter deve ser incrementado no swap
            pageInCounter++;
        }
        else
            reorder_pages(currentReference->reference);
        currentReference = currentReference->next;
    } while(currentReference != referencesString);
}

// Este algoritmo tem um bit de referência(BR) que retira a página quando BR==0
void run_secondChance(){
    reset_counters();
    currentPage = pagesInMemory;
    referenceString_t* currentReference = referencesString;
    do {
//        printf(".%d\n",currentReference->reference);
        if(search_page(currentReference) == 1){                
            pageFaultCounter++;
            swap_pagesForSecondChance(currentReference->reference);
            if(pageInCounter >= numberOfFrames)
                pageOutCounter++;
            pageInCounter++;
        }
        currentReference = currentReference->next;
    } while(currentReference != referencesString);
}

void run_improvedSecondChance(){
    reset_counters();
    currentPage = pagesInMemory;
    referenceString_t* currentReference = referencesString;
    do {
//        printf(".%d\n",currentReference->reference);
        if(search_page(currentReference) == 1){                
            pageFaultCounter++;
            swap_pagesForImprovedSecondChance(currentReference);
            if(pageInCounter >= numberOfFrames)
                pageOutCounter++;
            pageInCounter++;
        }
        currentReference = currentReference->next;
    } while(currentReference != referencesString);
}

// Procura pela page na pagesInMemory
// Entrada: newReference - referência da página procurada
// Saídas:  0    - página encontrada
//          1    - página não encontrada
int search_page(referenceString_t* newReference){
    pageInMemory_t* currentPage = pagesInMemory;
    do {
        if(currentPage->page == newReference->reference) {
            if(newReference->accessType == WRITE)  
                currentPage->modificationBit = 0;      
            return 0;            
        }
        currentPage = currentPage->next;
    } while(currentPage != pagesInMemory);
    return 1;
}

// Substitui páginas em pagesInMemory para o algoritmo FIFO
void swap_pages_FIFO(int newPage){
    currentPage->page = newPage;
    currentPage = currentPage->next;
}

// Substitui páginas em pagesInMemory para o algoritmo LRU
void swap_pages_LRU(int newPage){
    currentPage->page = newPage;
}

void reorder_pages(int page){
    while(currentPage != pagesInMemory->prev){
        if(currentPage->page == page) {
            if(currentPage == pagesInMemory)
                break;
            // retira nodo
            currentPage->prev->next = currentPage->next;
            currentPage->next->prev = currentPage->prev;
            // cola nodo na ultima posição
            pagesInMemory->prev->next = currentPage;
            currentPage->prev = pagesInMemory->prev;
            pagesInMemory->prev = currentPage;
            currentPage->next = pagesInMemory;            
            break;
        }
        currentPage = currentPage->next;
    }
    pagesInMemory = currentPage->next;
    currentPage = pagesInMemory;
}

void swap_pagesForSecondChance(int newPage){
    while(1){        
        if(currentPage->referenceBit == 0){
            currentPage->page = newPage;
            currentPage->referenceBit = 1;
            currentPage = currentPage->next;
            return;
        }
        else {
            currentPage->referenceBit = 0;            
            currentPage = currentPage->next;
        }  
    }
}

void swap_pagesForImprovedSecondChance(referenceString_t* newReference){
    pageInMemory_t* markedPageForChange = NULL;    
    while(1){                
        if(currentPage->referenceBit == 0){
            if(currentPage->modificationBit == 0 || currentPage == markedPageForChange){
                currentPage->page = newReference->reference;
                currentPage->referenceBit = 1;
                currentPage->modificationBit = 1;//se WRITE,mb = 0
                currentPage = currentPage->next;
                return;
            }
            else {
                if(markedPageForChange == NULL)
                    markedPageForChange = currentPage;
                currentPage = currentPage->next;
            }                
        }
        else {
            currentPage->referenceBit = 0;                     
            currentPage = currentPage->next;
        }  
    }
}

void print_analytics(){
    printf("pageIn: %d   pageOut: %d   pageFault: %d\n",pageInCounter,pageOutCounter,pageFaultCounter);
}

void print_pages(){
    currentPage = pagesInMemory;
    do {
        printf("p%d\n",currentPage->page);    
        currentPage = currentPage->next;
    } while(currentPage != pagesInMemory);
}

void reset_counters(){
    pageInCounter = 0;
    pageOutCounter = 0;
    pageFaultCounter = 0;
}
